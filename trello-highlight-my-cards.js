// ==UserScript==
// @name         Trello Highlight My Cards
// @version      1.1
// @author       Ran
// @match        https://trello.com/*
// ==/UserScript==

(function() {

    'use strict';

    const username = 'ran_tnl';
    const card_bg_color = '#fffb79';
    const board_bg_color = '#181818';

    const board = document.querySelector('#trello-root');

    function highlight() {
        // 移除看板背景，設為深色底
        board.classList.remove('body-light-board-background');
        board.removeAttribute('style');
        board.style.background = board_bg_color;

        // 取得所有列表
        let lists = board.querySelectorAll('.js-list');

        lists.forEach(function (list) {
            // 列表名稱
            let list_name = list.querySelector('.list-header-name').innerHTML;

            if (list_name.includes('待執行') || list_name.includes('執行中')) {
                // 移除列表樣式
                list.removeAttribute('style');
            } else {
                // 更改列表透明度
                list.style.opacity = '0.2';
            }
        });

        // 取得所有卡片
        let cards = board.querySelectorAll('.list-card');

        cards.forEach(function (card) {
            // 是否已加入卡片
            let is_in_card = card.querySelectorAll('img[title*="' + username + '"]').length > 0;

            if (is_in_card) {
                // 更改卡片顏色
                card.style.background = card_bg_color;
            } else {
                // 移除卡片樣式
                card.removeAttribute('style');
            }
        });
    }

    const config = {
        attributes: true,
        attributeFilter: ['style'],
        childList: true,
        subtree: true
    };

    const callback = async (mutationsList, observer) => {
        // 停止監聽
        observer.disconnect();

        // 將我的卡片顯著加亮
        await highlight();

        // 重新監聽
        observer.observe(board, config);
    };

    const observer = new MutationObserver(callback);

    // 監聽看板卡片異動
    observer.observe(board, config);

})();