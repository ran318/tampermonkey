// ==UserScript==
// @name         Greenroom Show My Folders Only
// @version      1.0
// @author       Ran
// @match        https://greenroom.tnlmedia.com/tuber/
// ==/UserScript==

(function() {

    'use strict';

    let resetFolder = document.querySelector('#resetFolder');
    let optgroups = resetFolder.querySelectorAll('optgroup');

    optgroups.forEach(function (optgroup) {
        if (optgroup.label != 'Ran') {
            optgroup.remove();
        }
    });

})();